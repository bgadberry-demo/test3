# Onboarding Quick Guide


This project contains assignable issues of the most relevant and valuable resources during your onboarding with GitLab. We greatly appreciate your feedback as we continue to iterate.

# Structure of the issues


* Authentication
* Setting-Up Group and Project Hierarchy
* Import/Export
* GitLab Instance - Security Best Practices
* Monitoring GitLab
* Backing-up GitLab
* How We Guarantee Backups (SaaS)
* Alternative Backup Strategies
* Support Information (Self-Managed)
* Support Information (SaaS)
* API Intro + Rate Limits (Self-Managed)
* API Intro + Rate Limits (SaaS)
* Where To Get Training
* Provide Feedback

# How to use this project

Within your GitLab, create a `New project`, select the `Import project/repository` option, and select `Repo by URL` as your import option. Use `https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/onboarding-quick-guide` as the Git repository URL.

Next, complete the project name, project description (optional), select your desired visibility level. Then, click `Create project`.

After the project imports, navigate to the left-hand panel and click on Issues. The list of issues is a checklist for your initial onboarding with GitLab and provides consolidated information to get you started. For additional information, our [GitLab Docs](https://docs.gitlab.com/) has a wealth of information.

# How to provide feedback

Please use this [survey link](https://forms.gle/DuyJHaZyyq2ke4paA) to provide feedback. The survey link is also within the last issue titled, "Provide Feedback".
